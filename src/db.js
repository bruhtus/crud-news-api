import 'regenerator-runtime/runtime'
import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'

const mongod = new MongoMemoryServer()

// connect to database
const connect = async () => {
  const uri = await mongod.getUri()
  const mongooseOpts = {
    useNewUriParser: true,
    useUnifiedTopology: true,
    poolSize: 10,
  }

  await mongoose.connect(uri, mongooseOpts)
}

// disconnect and close connection
const closeDatabase = async () => {
  await mongoose.connection.dropDatabase()
  await mongoose.connection.close()
  await mongod.stop()
}

// clear the database
const clearDatabase = async () => {
  const collections = mongoose.connection.collections
  for (const key in collections) {
    const collection = collections[key]
    await collection.deleteMany()
  }
}

export default { connect, closeDatabase, clearDatabase }
