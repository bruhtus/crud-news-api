import 'regenerator-runtime/runtime'
import request from 'supertest'
import app from '../server'
import * as db from './db'

beforeAll(async () => await db.connect)
afterEach(async () => await db.clearDatabase)
afterEach(async () => await db.closeDatabase)

describe('check route exist', () => {
  test('can create news', async () => {
    const res = await request(app)
      .post('/api/news')
      .send({ title: 'jakarta banjir', content: 'masih tenggelam' })
    expect(res.statusCode).toBe(200)
    expect(res.body).toStrictEqual({ title: 'jakarta banjir', content: 'masih tenggelam' })
  })
})
